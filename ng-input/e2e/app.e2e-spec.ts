import { NgCustomLibPage } from './app.po';

describe('ng-custom-lib App', () => {
  let page: NgCustomLibPage;

  beforeEach(() => {
    page = new NgCustomLibPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
