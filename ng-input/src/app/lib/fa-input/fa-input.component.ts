import {AfterContentInit, Component, ContentChild, ContentChildren, HostBinding, Input, OnInit} from '@angular/core';
import { InputDirective } from '../common/input.directive';

@Component({
  selector: 'app-fa-input',
  templateUrl: './fa-input.component.html',
  styleUrls: ['./fa-input.component.sass']
})

export class FaInputComponent implements  AfterContentInit {
  @Input()
  icon: string;

  @ContentChild(InputDirective)
  input: InputDirective;

  constructor() { }

  ngAfterContentInit() {
    if (!this.input) {
      console.error('the au-fa-input needs an input inside it\'s content');
    }
  }

  @HostBinding('class.input-focus')
  get isInutFocus(): boolean {
    return this.input ? this.input.focus : false;
  }

  get classes(): Object {
    const cssClasses = {};
    if (this.icon) {
      cssClasses[`fa-${this.icon}`] = true;
    }
    return cssClasses;
  }
}
