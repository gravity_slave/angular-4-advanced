import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: 'input'
})
export class InputDirective {
  public focus = false;

  constructor() { }

  @HostListener('focus')
  onFocus() {
    this.focus = true;
  }

  @HostListener('blur')
  onBlur() {
    this.focus = false;

  }

}
