import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FaInputComponent } from './fa-input/fa-input.component';
import { InputDirective } from './common/input.directive';
import { MAInputComponent } from './ma-input/ma-input.component';

@NgModule({
  declarations: [ FaInputComponent, InputDirective, MAInputComponent ],
  imports: [ CommonModule ],
  exports: [ FaInputComponent, InputDirective, MAInputComponent ]
})

export class FAInputModule {}
