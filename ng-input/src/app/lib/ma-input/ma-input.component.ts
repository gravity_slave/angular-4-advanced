import { AfterContentInit, Component, ContentChild, HostBinding, Input } from '@angular/core';
import { InputDirective } from '../common/input.directive';

@Component({
  selector: 'app-md-input',
  templateUrl: './ma-input.component.html',
  styleUrls: ['./_md-input.component.sass']
})
export class MAInputComponent implements AfterContentInit {
  @Input()
  icon: string;

  @ContentChild(InputDirective)
  input: InputDirective;
  constructor() { }

  ngAfterContentInit() {
    if (!this.input) {
      console.error('the md input needs an input inside its component');
    }
  }

  @HostBinding('class.input-focus')
  get InputFocus() {
    return this.input ? this.input.focus : false;
  }
}
