import {TestBed, async, ComponentFixture} from '@angular/core/testing';

import { AppComponent } from './app.component';
import { FaInputComponent } from './lib/fa-input/fa-input.component';
import { InputDirective } from './lib/common/input.directive';
import {DebugElement} from "@angular/core";
import {By} from "@angular/platform-browser";

describe('AppComponent', () => {

  let component: AppComponent,
    fixture: ComponentFixture<AppComponent>,
    el: DebugElement,
    emailField: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent, FaInputComponent, InputDirective
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
     fixture = TestBed.createComponent(AppComponent),
     component = fixture.debugElement.componentInstance,
     el = fixture.debugElement,
     emailField = el.query(By.css('#email-field'));
     fixture.detectChanges();

  });

  it('should create the app', async(() => {
    expect(component).toBeTruthy();
  }));

  it('should create an email input', async( () => {
    expect(emailField).toBeTruthy();
  }));

  it('should include the correct email icon inside the email input', async(() => {
    expect(emailField.query(By.css('i.icon.fa.fa-envelope'))).toBeTruthy();
  }));

  it('should have projected the correct test input inside the email field', async(() => {
    expect(emailField.query(By.css('input.test-class'))).toBeTruthy();
  }));

});
