import { NgTabPanelPage } from './app.po';

describe('ng-tab-panel App', () => {
  let page: NgTabPanelPage;

  beforeEach(() => {
    page = new NgTabPanelPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
