import {TestBed, async, ComponentFixture} from '@angular/core/testing';

import { AppComponent } from './app.component';
import {TabComponent} from "./tab/tab.component";
import {TabPanelComponent} from "./tab-panel/tab-panel.component";
import {DebugElement} from "@angular/core";
import {By} from "@angular/platform-browser";


describe('AppComponent', () => {
  let component: AppComponent,
    fixture: ComponentFixture<AppComponent>,
    el: DebugElement,
    tabpanel: DebugElement;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent, TabComponent, TabPanelComponent
      ],
    }).compileComponents();
  }));

  beforeEach(async(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.debugElement.componentInstance;
     el = fixture.debugElement;
     tabpanel =  el.query(By.css('#tab-panel'));
     fixture.detectChanges();
  }));

  it('should create the app', async(() => {
    expect(component).toBeTruthy();
  }));

  it('should find only one tab inside the tab container', async(() => {
    const tabs = tabpanel.queryAll(By.css('.tab'));
    expect(tabs).toBeTruthy();
    expect(tabs.length).toBe(1);
  }));

  it('should find the Contact button marked as active', async(() => {
    const selectedButton = tabpanel.query(By.css('.tab-panel-buttons li.selected')).nativeElement;
    expect(selectedButton).toBeTruthy();
    expect(selectedButton.textContent).toBe('Contact');
  }))

  it('should display the Contacts tab', async(() => {
    const contactEmail = tabpanel.query(By.css('.contact-email')).nativeElement;
    expect(contactEmail).toBeTruthy();
  }))

  it('should switch to login tab', () => {
    const tabButtons = tabpanel.queryAll(By.css('.tab-panel-buttons li'));
    tabButtons[0].nativeElement.click();
    fixture.detectChanges();
    const loginEmail= tabpanel.query(By.css('.login-email'));
    expect(loginEmail).toBeTruthy();
    const select = tabpanel.query(By.css('.tab-panel-buttons li.selected')).nativeElement;
    expect(select).toBeTruthy();
    expect(select.textContent).toBe('Login');
  });

});
