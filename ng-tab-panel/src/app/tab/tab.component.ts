import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.sass']
})
export class TabComponent implements OnInit {
  @Input()
  title: string;

  @Input()
  public selected: boolean = false;
  constructor() { }

  ngOnInit() {
  }

}
