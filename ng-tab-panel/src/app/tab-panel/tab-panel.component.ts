import {AfterContentInit, Component, ContentChildren, Input, QueryList, TemplateRef, ViewChild} from '@angular/core';
import { TabComponent } from "../tab/tab.component";

@Component({
  selector: 'app-tab-panel',
  templateUrl: './tab-panel.component.html',
  styleUrls: ['./tab-panel.component.sass']
})
export class TabPanelComponent implements AfterContentInit {

  @ContentChildren(TabComponent)
  tabs: QueryList<TabComponent>;

  @Input()
  headerTemplate: TemplateRef<any>;

  constructor() { }

  ngAfterContentInit() {
    const selectedTab= this.tabs.find(tab => tab.selected);
    if (!selectedTab && this.tabs.first) {
      this.tabs.first.selected = true;
    }
  }

  public selectTab(tab: TabComponent): void {
    this.tabs.forEach(tab => tab.selected = false);
    tab.selected = true;
  }

  get tabsContext() {
    return {
      tabs: this.tabs
    }
  }
}
